# Neos website

## About

This repository contains the public website netzwerk-nrw.info, based on the Neos CMS.
There are many custom _node types_ included to provide event management and registration functionalities.

*This website is a public template for all networks that can benefit from it. Just fork this repo, follow the instructions of setting up the development/hosting environment and start building your own public site and functionality!*

### Structure

This repository only contains the custom node types and configurations as a "site package" (beneath "DistrubutionPackages") and the dependant packages for Neos installation. The `composer.json` file defines the dependencies for the Neos CMS.
The `Dockerfile` file holds the information for the Docker engine to build an Docker image (and container), based on Linux + PHP 7.3 + composer.

## Set-up

In general, follow the instructions of the ["_Path Repository_ Setup"](https://docs.neos.io/cms/manual/dependency-management#the-path-repository-setup).

### Development environment

_coming soon..._

### Production hosting

It is recommended to use Docker on production. But it is also possible to fork the repository on a bare-metal server without Docker engine installed - then you have to install the following software pacakges by your own (no guarantees that this list is complete and working):
* Webserver like Nginx or Apache2 (with mysql protocol support enabled)
* PHP > 7.3 (included as _module_ into the webserver so `.php` files can be handled by the PHP interpretor)
* composer (PHP package manager/installer)
* MariaDB or MySQL (for compatible versions see the offical Neos documentation!)
