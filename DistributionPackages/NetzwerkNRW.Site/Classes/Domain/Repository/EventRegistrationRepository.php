<?php
namespace NetzwerkNRW\Site\Domain\Repository;

/*
 * This file is part of the NetzwerkNRW.Site package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class EventRegistrationRepository extends Repository
{
}
