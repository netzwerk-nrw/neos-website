<?php
namespace NetzwerkNRW\Site\Controller;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;

/**
 * Controller for asset handling
 *
 * @Flow\Scope("singleton")
 */
class BackendController extends ActionController {

    public function indexAction() {
        // TODO: retrieve all NodeTypes with Type "NetzwerkNRW.Site:Document.Event"
        $this->view->assign('events', []);
    }

}
