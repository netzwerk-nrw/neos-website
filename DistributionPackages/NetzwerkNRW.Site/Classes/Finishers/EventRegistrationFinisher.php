<?php
namespace NetzwerkNRW\Site\Finishers;

use Neos\Flow\Annotations as Flow;
use NetzwerkNRW\Site\Domain\Model\EventRegistration;
use NetzwerkNRW\Site\Domain\Repository\EventRegistrationRepository;
use Neos\Form\Core\Model\FormElementInterface;

/**
 * This finisher persists the data from the event registration form.
 *
 */
class EventRegistrationFinisher extends \Neos\Form\Core\Model\AbstractFinisher
{

    /**
     * @var array
     */
    protected $defaultOptions = array(
        'db' => true,
        'elasticSearch' => false
    );

    /**
     * @Flow\Inject
     * @var EventRegistrationRepository
     */
    protected $eventRegistrationRepository;

    /**
     * @var \Neos\Flow\Security\Context
     */
    protected $securityContext;

    /**
     * Injects the security context
     *
     * @param \Neos\Flow\Security\Context $securityContext The security context
     * @return void
     */
    public function injectSecurityContext(\Neos\Flow\Security\Context $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * Executes this finisher
     * @see AbstractFinisher::execute()
     *
     * @return void
     * @throws \Neos\Form\Exception\FinisherException
     */
    protected function executeInternal()
    {
        $formRuntime = $this->finisherContext->getFormRuntime();
        $formValues = json_encode($formRuntime->getFormState()->getFormValues());
        $formIdentifier = $formRuntime->getFormDefinition()->getIdentifier();
        $accountIdentifier = $this->getAccountIdentifier();
        $formData = new EventRegistration($formIdentifier, $accountIdentifier, $formValues);

        var_dump($formRuntime, "formRuntime");
        var_dump($formIdentifier, "formIdentifier");
        var_dump($formValues, "formValues");
        var_dump($formData, "formData");
        var_dump($accountIdentifier, "accountIdentifier");
        die();
        /*
        *** Simple ***
        $this->eventRegistrationRepository->add($formData);
        */

        /*
        *** Extended (Key-Value Map) ***
        $properties = array();
        $elements = $formRuntime->getFormDefinition()->getRenderablesRecursively();
        foreach ($elements as $element) {
            if (!$element instanceof FormElementInterface) {
                continue;
            }
            $properties[$element->getIdentifier()] = $element->getProperties();
        }
        $this->elasticSearchService->index($formData, $properties);
        */
    }

    /**
     * @return null|string
     */
    protected function getAccountIdentifier()
    {
        $accountIdentifier = null;
        if ($this->securityContext !== null && $this->securityContext->canBeInitialized()) {
            $account = $this->securityContext->getAccount();
            if ($account !== null) {
                $accountIdentifier = $account->getAccountIdentifier();
            }
        }
        return $accountIdentifier;
    }
}
